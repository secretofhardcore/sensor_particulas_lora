# Particulate Matter sensor for The Things Network (LoRaWAN)

This project contains the source code and documentation for development of
a wireless particulate matter sensor on air for PM1.0 PM2.5, PM4.0 and PM10.0.

## Hardware Support

 - Teensy 3.1/3.2
 - Arduino UNO*
 - Adafruit HUZZAH ESP8266 o ESP-12
 
**Arduino UNO**: On this board the Sketch uses almost the whole memory,
additional features could be hard to add. The same serial port used to
program the board is the only capable of support the baud-rate used by
the Sensirion SPS30. This means that is necesary to disconnect the sensor
when programming the Sketch and connect it again on normal operation.
To debug the Sketch we use an additional SoftSerial port on the digital
pins 2 (RXIN) and 3 (TXOUT) with an external USB-Serial converter.

**Adafruit HUZZAH ESP8266 o ESP-12**: On ESP8266 based boards we have
a simmilar limitation to the Arduino UNO. The serial port used to program
the board is used by the particulate matter sensor. Is not possible to
debug directly this board using USB. To debug you'll need to use a
USB-Serial converter connected to the digital pin 0 (TXOUT).

## BOM
To build this project you'll need:

 - Any of the following boards:

   - [Teensy 3.1 o 3.2](https://www.pjrc.com/store/teensy32.html) 
   - [Arduino UNO](https://store.arduino.cc/usa/arduino-uno-rev3/). You'll also need two [voltage level converter from sparkfun](https://www.sparkfun.com/products/12009) or two [TBX0104](https://www.sparkfun.com/products/11771).
   - [Adafruit HUZZAH or ESP12](https://store.arduino.cc/usa/arduino-uno-rev3/). You'll also need one [voltage level converter from sparkfun](https://www.sparkfun.com/products/12009) or [TBX0104](https://www.sparkfun.com/products/11771).

 - [LoRa RFM95W](https://www.adafruit.com/product/3072) Radio

 - Particulate matter sensor [Sensirion SPS30](https://www.sparkfun.com/products/15103)

## Software Requirements

 - A recent version of [Arduino IDE](https://www.arduino.cc/en/Main/Software)
 - Library [Arduino-LMIC of MCCI-Catena](ttps://github.com/mcci-catena/arduino-lmic)
 
### Configuration settings for Arduino-LMIC

Install the library Arduino-LMIC inside the folder:

Linux:

    $HOME/Arduino/libraries

Windows:

    C:\Users\<Nombre de Usuario>\Documents\Arduino\libraries

Arduino-LMIC library requires the regional configuration for LoRaWAN to
be set. This is done modifying the header file ***lmic_project_config.h***
that you can found on the following folder:

Linux:

    $HOME/Arduino/libraries/arduino-lmic/project_config

Windows:

    C:\Users\<Username>\Documents\Arduino\libraries\arduino-lmic\project_config

Remove the comment from the line corresponding to your LoRaWAN region. In el
El Salvador the region is *Australia 915 (CFG_au921)*.

    // project-specific definitions
    //#define CFG_eu868 1
    //#define CFG_us915 1
    #define CFG_au921 1
    //#define CFG_as923 1
    // #define LMIC_COUNTRY_CODE LMIC_COUNTRY_CODE_JP       /* for as923-JP */
    //#define CFG_in866 1
    #define CFG_sx1276_radio 1
    //#define LMIC_USE_INTERRUPTS

## Board configuration

Board will be automatically detected at compile-teime. However if you want
to review the board configurations or add support for another different
board please check de file "sensirion_arch_config.h".

**Notes:** At this time only Teensy 3.1/3.2, Arduino UNO and ESP-12 are 
supported.

## Sketch Configuration
Open the file "ttn_config_example.h" and fill with the required values. 
Configurations for your device must be get from [The Things Network Console](https://console.thethingsnetwork.org/).

**Important:** Make sure you are using **ABP** activation and the option
**Frame Counter Checks** is disabled on the settings.

Copy all the parameters as indicated replaced **FILL_ME_IN** with the 
values get from The Things Network Console:

    // Copia el valor de Network Sessión Key en "FILL_ME_IN"
    #define TTN_NWKSKEY  FILL_ME_IN
    // Copia el valor de Application Sessión Key en "FILL_ME_IN"
    #define TTN_APPKEY   FILL_ME_IN
    // Copia el valor de Device Address en "FILL_ME_IN" anteponiendo 0x
    #define TTN_DEVADDR  0xFILL_ME_IN

Change the message interval on the following variable

    // Establece el intérvalo de envío de mensajes
    #define MSG_INTERVAL 60*12*1000

**Important**: It is expected that The Things Network users comply with the
message limit (airtime) allowed for their LoRaWAN regions. On this example
a message is sent around every 12 minutes. To estimate how many messages
are allowed as a maximum by day you can check over:

https://avbentem.github.io/lorawan-airtime-ui/ttn/eu868

Please use a *payload of 18 bytes* and select your coresponding LoRaWAN
region.


### Debug configuration
When DEBUG_TTN is enabled on the configuration, debug messages are sent
over the DEBUG_PORT. This is really useful to identify any troubles. However
the messsage interval is reduced to 10 seconds. Do not enable DEBUG_TTN
on production devices because you will infringe the local LoRaWAN limitations.

To enable the debug mode un-comment the following line

    //#define TTN_DEBUG

### The Things Network payload decoder
Utiliza el siguiente decodificador para extraer los datos de los mensajes
enviados por el sensor. Este código se agrega en la pestaña correspondiente
de la aplicación LoRaWAN en la consola de The Things Network:

    function byte_array_to_int(byte_array) {
      var value = (byte_array[3]<<24) | (byte_array[2]<<16) | (byte_array[1]<<8) | byte_array[0]
      return value
    }

    function Decoder(bytes, port) {
      // Decode an uplink message from a buffer
      // (array) of bytes to an object of fields.
      var decoded = {};
    
      // if (port === 1) decoded.led = bytes[0];
      if(bytes.length==18) {
        if(bytes[0]===0) {
          decoded.pm1p0 = byte_array_to_int([bytes[1],bytes[2],bytes[3],bytes[4]])/100000.0;
          decoded.pm2p5 = byte_array_to_int([bytes[5],bytes[6],bytes[7],bytes[8]])/100000.0;
          decoded.pm4p0 = byte_array_to_int([bytes[9],bytes[10],bytes[11],bytes[12]])/100000.0;
          decoded.pm10p0 = byte_array_to_int([bytes[13],bytes[14],bytes[15],bytes[16]])/100000.0;
          decoded.accurate = (bytes[17]===0) ? true : false;
        }
      }
    
      return decoded;
    }

You can check if the decoder is working using the following payload:

    0032AF11000F2718005F241D00172D1F0000

You should have the following result:

    {
      "accurate": true,
      "pm1p0": 11.58962,
      "pm10p0": 20.43159,
      "pm2p5": 15.82863,
      "pm4p0": 19.09855
    }

## Hardware connections
Connect the RFM95W radio as is shown in the following table:

**Teensy 3.1/3.2**

| Teensy | RFM95W |
|--------|--------|
|  3.3V  |  3.3V  |
|  GND   |  GND   |
|  9     |  RESET |
|  10    |  NSS   |
|  11    |  MOSI  |
|  12    |  MISO  |
|  13    |  SCK   |
|  14    |  DIO0  |
|  15    |  DIO1  |
|  16    |  DIO2  |

**Arduino UNO**

Arduino UNO requires two voltage level converters. Please check the 
documentation of your converter for more details. The converter must
support voltage levels of 3.3V and 5V.


|  UNO   |  Conv 5V   |  Conv 3V3  | RFM95W |
|--------|------------|------------|--------|
|  5V    |  5V        |  NC        | NC     |
|  3.3V  |  NC        |  3V3       | 3V3    |
|  GND   |  GND       |  GND       | GND    |
|  9     |  IOHV1     |  IOLV1     | RESET  |
|  10    |  IOHV2     |  IOLV2     | NSS    |
|  12    |  IOHV3     |  IOLV3     | MOSI   |
|  11    |  IOHV4     |  IOLV4     | MISO   |
|  13    |  IOHV5     |  IOLV5     | SCK    |
|  6     |  IOHV6     |  IOLV6     | DIO0   |
|  7     |  IOHV7     |  IOLV7     | DIO1   |
|  8     |  IOHV8     |  IOLV8     | DIO2   |

You could use the [voltage level converter from sparfun](https://www.sparkfun.com/products/12009) or [TBX0104](https://www.sparkfun.com/products/11771)
as voltage level converters.

**Adafruit HUZZAH o ESP-12**

| ESP12  | RFM95W |
|--------|--------|
|  3.3V  |  3.3V  |
|  GND   |  GND   |
|  5     |  RESET |
|  4     |  NSS   |
|  13    |  MOSI  |
|  12    |  MISO  |
|  14    |  SCK   |
|  16    |  DIO0  |
|  0     |  DIO1  |

DIO2 is not connected on the ESP-12. There is not enough pins available.

Connect the Sensirion SPS30 as shown:

**Teensy 3.1/3.2**

| Teensy | Sensirion SPS30 |
|--------|-----------------|
|  Vin   |       VDD       |
|  GND   |       GND       |
|  0     |       TX        |
|  1     |       RX        |
|  N/C   |       SEL       |

**Arduino UNO**

| UNO    | Sensirion SPS30 |
|--------|-----------------|
|  5V    |       VDD       |
|  GND   |       GND       |
|  0     |       TX        |
|  1     |       RX        |
|  N/C   |       SEL       |

In Arduino UNO you must disconnect the sensor each time you program the board.

**Adafruit HUZZAH o ESP-12**

The ESP-12 requires a voltage level converter to connect the Sensirion 
SPS30 it must support 3.3V and 5V level conversion.

|  UNO   |  Conv 5V   |  Conv 5V   | Sensirion SPS30 |
|--------|------------|------------|-----------------|
|  5V    |  5V        |  NC        |       NC        |
|  3.3V  |  NC        |  3V3       |       3V3       |
|  GND   |  GND       |  GND       |       GND       |
|  1     |  IOLV1     |  IOHV1     |       TX        |
|  3     |  IOLV2     |  IOHV2     |       RX        |

**Notes:** Remember that on the ESP-12 the voltage level converter uses
the voltage level converter to connect the Sensirion SPS30. As in the
Arduino UNO you must disconnect the sensor each time you program the board.

## Copyright
This Sketch has been written by Mario Gomez from the Hackerspace San Salvador
and uses several libraries and components of third parties. For more info
about the licenses used, please check the headers over each source file.
